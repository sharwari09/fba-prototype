from twisted.internet.protocol import DatagramProtocol
from twisted.internet.task import LoopingCall
from twisted.internet import reactor
from enum import Enum
import traceback
import pickledb
import sys

db_file = None

node_list = [3000, 3001, 3002, 3003]
quorum_slices = {
    3000: [3000, 3001],
    3001: [3001, 3002],
    3002: [3002, 3003],
    3003: [3003, 3000]
}


class State(Enum):
    UNCOMMITTED = 0
    VOTED = 1
    ACCEPTED = 2
    CONFIRMED = 3


class Node():

    def __init__(self, nodeid, quorum_slice, quorum, db):
        self.state = State.UNCOMMITTED
        self.nodeid = nodeid
        self.statement = None
        self.writerlist = []
        self.reader = None
        self.votelist = []
        self.acceptlist = []
        self.quorum_slice = quorum_slice
        self.quorum = quorum
        self.db = db
        self.client = None
        self.initWriters()

    def __del__(self):
        print("Cleaning up")

    def initWriters(self):
        for _eachport in self.quorum:
            if _eachport == self.nodeid:
                continue
            writer = MulticastWriterUDP()
            writer.initWriter(self, _eachport)
            reactor.listenMulticast(_eachport, writer,
                                    listenMultiple=True)
            self.addWriter(writer)

    def addWriter(self, writer):
        if writer not in self.writerlist:
            self.writerlist.append(writer)

    def setReader(self, reader):
        self.reader = reader

    def checkState(self):
        msg = None
        if self.state not in [State.CONFIRMED, State.UNCOMMITTED]:
            print("Votelist: {}\tAcceptlist: {}".format(self.votelist, self.acceptlist))
        if self.state == State.VOTED:
            msg = "vote,{},{}".format(self.nodeid, self.statement)
            if len(self.votelist) >= (len(self.quorum_slice)-1):
                self.accept()
                msg = "accept,{},{}".format(self.nodeid, self.statement)
        elif self.state == State.ACCEPTED:
            msg = "accept,{},{}".format(self.nodeid, self.statement)
            if len(self.acceptlist) >= (len(self.quorum)-1):
                self.confirm()
                msg = "confirm,{},{}".format(self.nodeid, self.statement)
        elif self.state == State.CONFIRMED:
            msg = "confirm,{},{}".format(self.nodeid, self.statement)

        return msg

    def sendRequestsToReplicas(self, msg):
        msg = "request,{},{}".format(self.nodeid, msg)
        for _ in range(0,1):
            for _writer in self.writerlist:
                _writer.sendMsg(msg)

    def endTransaction(self, ctx):
        for _writer in self.writerlist:
            _writer.stopRunning()
        self.reader.stopReader()

    def sendReplyToClient(self, msg):
        if self.client:
            print("Sending {} to client on {}".format(msg, self.client))
            self.reader.sendToClient(msg, self.client)
        self.reader.markForDelete()

    def receivedRequest(self, sender, msg, address):
        if self.state != State.UNCOMMITTED:
            return None
        print("receivedRequest: {} from {}".format(msg, sender))
        if self.client is None and sender == 0:
            self.client = address
            self.sendRequestsToReplicas(msg)
        self.statement = msg
        assert(len(self.votelist) == 0)
        self.vote()
        return True

    def vote(self):
        if self.nodeid not in self.votelist:
            self.votelist.append(self.nodeid)
        self.state = State.VOTED

    def receivedVote(self, sender, msg):
        if self.statement != msg:
            return None
        if (sender in self.quorum_slice) and (sender not in self.votelist):
            print("receivedVote: {}:{}".format(sender, msg))
            self.votelist.append(sender)
        return True

    def accept(self):
        if self.nodeid not in self.acceptlist:
            self.acceptlist.append(self.nodeid)
        self.state = State.ACCEPTED

    def receivedAccept(self, sender, msg):
        if self.statement != msg:
            return None
        if (sender in self.quorum_slice) and (sender not in self.votelist):
            self.votelist.append(sender)
        if (sender in self.quorum_slice) and (sender not in self.acceptlist):
            print("receivedAccept: {}:{}".format(sender, msg))
            self.acceptlist.append(sender)
        return True

    def processMessage(self, message, address):
        op, sender, msg = message.split(',')
        sender = int(sender)
        ret = False
        if op == "request":
            ret = self.receivedRequest(sender, msg, address)
        elif op == "vote":
            ret = self.receivedVote(sender, msg)
        elif op == "accept":
            ret = self.receivedAccept(sender, msg)
        elif op == "confirm":
            ret = self.receivedConfirm(sender, msg)
        return ret

    def confirm(self):
        if self.state in [State.UNCOMMITTED, State.CONFIRMED]:
            return
        key, value = self.statement.replace('$', '').split(':')
        value = int(value)
        value = self.writeToDB(key, value)
        self.printDB()
        self.state = State.CONFIRMED
        msg = "reply,{},{}:{}".format(self.nodeid, key, value)
        self.sendReplyToClient(msg)

    def receivedConfirm(self, sender, msg):
        if self.statement != msg:
            return None
        if (sender in self.quorum_slice) and (sender not in self.votelist):
            self.votelist.append(sender)
        if (sender in self.quorum_slice) and (sender not in self.acceptlist):
            print("receivedConfirm: {}:{}".format(sender, msg))
            self.acceptlist.append(sender)
        return True

    def writeToDB(self, key, value):
        print("Writing {}, {} to DB".format(key, value))
        if self.db.exists(key):
            value = int(self.db.get(key)) + int(value)
        self.db.set(key, value)
        self.db.dump()
        return value

    def printDB(self):
        print("User Wallet Info:")
        keys = self.db.getall()
        for key in keys:
            print("{}:${}".format(key, self.db.get(key)))

#################################################

class MulticastWriterUDP(DatagramProtocol):
    def initWriter(self, node, port):
        self.node = node
        self.port = port

    def startProtocol(self):
        self.transport.joinGroup('224.0.0.1')
        self.loopObj = LoopingCall(self.sendState)
        self.loopObj.start(1, now=False)

    def stopRunning(self):
        self.loopObj.stop()
        del self.node

    def sendState(self):
        try:
            msg = self.node.checkState()
            self.sendMsg(msg)
        except Exception as e:
            print("Caught exception {}: {}".format(e, traceback.format_exc()))

    def sendMsg(self, msg):
        if msg:
            self.transport.write(msg.encode(), ('224.0.0.1', self.port))


class MulticastReaderUDP(DatagramProtocol):
    def setNode(self, node):
        self.node = node

    def startProtocol(self):
        self.transport.joinGroup('224.0.0.1')
        self.node = None

    def datagramReceived(self, datagram, address):
        global port, node_list, db
        if self.node is None and datagram.decode('utf-8').startswith('request'):
            currNode = Node(nodeid=port, quorum_slice=node_list,
                            quorum=node_list, db=db)

            currNode.setReader(self)
            self.setNode(currNode)

        if self.node:
            self.node.processMessage(datagram.decode('utf-8'), address)

    def markForDelete(self):
        reactor.callLater(2.5, self.node.endTransaction, None)

    def stopReader(self):
        if self.node is not None:
            del self.node
            self.node = None

    def sendToClient(self, message, clientAddress):
        self.transport.write(message.encode(), clientAddress)


if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.exit(1)

    port = int(sys.argv[1])
    db_file = 'assignment3_{}.db'.format(port)
    db = pickledb.load(db_file, False)

    reader = MulticastReaderUDP()
    print("Starting listener on {}".format(port))
    reactor.listenMulticast(port, reader, listenMultiple=True)

    reactor.run()
