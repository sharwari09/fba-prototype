from twisted.internet.protocol import DatagramProtocol
from twisted.internet.task import LoopingCall
from twisted.internet import reactor
from enum import Enum
import time
import sys

message_list = ['foo:$10', 'bar:$30', 'foo:$20',
                'bar:$20', 'foo:$30', 'bar:$10']

class ClientState(Enum):
    INIT = 0
    SENDING_MSG = 1
    AWAITING_REPLY = 2
    REPLY_RECEIVED = 3

class Client(DatagramProtocol):
    def initClient(self, self_port, dest_ip, dest_port):
        self.port = self_port
        self.dest_ip = dest_ip
        self.dest_port = dest_port
        self.state = ClientState.INIT
        self.msg_id = 0

    def getNextMsg(self):
        if self.msg_id == len(message_list):
            return None
        msg = message_list[self.msg_id % len(message_list)]
        self.msg_id += 1
        return msg

    def checkAndSendMessage(self):
        if self.state not in [ClientState.INIT, ClientState.REPLY_RECEIVED]:
            return
        self.state = ClientState.SENDING_MSG

        msg = self.getNextMsg()
        if msg is None:
            self.loopObj.stop()
            self.stopProtocol()
            return
        message = "request,{},{}".format(self.port, msg)
        print("Sending {} to {}:{}".format(message, self.dest_ip, self.dest_port))
        self.transport.write(message.encode())
        self.state = ClientState.AWAITING_REPLY

    def startProtocol(self):
        self.transport.connect(self.dest_ip, self.dest_port)
        self.loopObj = LoopingCall(self.checkAndSendMessage)
        self.loopObj.start(5, now=False)

    def datagramReceived(self, datagram, addr):
        print("Received {} from {}".format(datagram, addr))
        if datagram.decode('utf-8').startswith('reply'):
            time.sleep(5)
            self.state = ClientState.REPLY_RECEIVED

dest_ip = '127.0.0.1'
dest_port = int(sys.argv[1])
self_port = 0

client = Client()
client.initClient(self_port, dest_ip, dest_port)

reactor.listenUDP(self_port, client)
reactor.run()

